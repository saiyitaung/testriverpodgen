import 'dart:convert';

import 'package:rivergentest/product_response.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:http/http.dart' as http;
part 'product_provider.g.dart';
const baseUrl="https://dummyjson.com";
@riverpod
Future<Product> productDetail(ProductDetailRef ref,
    {required int productId}) async {
  final resp = await http.Client()
      .get(Uri.parse("$baseUrl/products/$productId"));
  final pr = Product.fromJson(jsonDecode(resp.body));
  return pr;
}

@Riverpod(keepAlive: true)
class ShowBackToTop extends _$ShowBackToTop {
  @override
  bool build() {
    return false;
  }

  void setShow() {
    state = true;
  }

  void setFalse() {
    state = false;
  }
}

const totalProduct = 100;

@riverpod
class ProductList extends _$ProductList {
  List<Product> products = [];
  bool busy = false;

  @override
  Future<List<Product>> build() async {
    final resp =
        await http.Client().get(Uri.parse("$baseUrl/products"));
    final pr = ProductResponse.fromJson(jsonDecode(resp.body));
    products = pr.products ?? [];
    return products;
  }

  void addProduct() async {
    state = AsyncData(products);
    await Future.delayed(const Duration(seconds: 2));
    products.add(Product(title: "Demo"));
    state = await Future.delayed(
        const Duration(seconds: 2), () => AsyncData(products));
  }

  void loadMore() async {
    if (!busy) {
      busy = true;
      final len = products.length;
      final resp = await http.Client()
          .get(Uri.parse("$baseUrl/products?skip=$len"));
      final pr = ProductResponse.fromJson(jsonDecode(resp.body));
      products = [...products, ...pr.products ?? []];
      state = AsyncData(products);
      busy = false;
    }
  }
}
