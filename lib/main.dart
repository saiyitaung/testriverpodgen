import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:loading_indicator/loading_indicator.dart';
import 'package:rivergentest/detail_page.dart';
import 'package:rivergentest/product_provider.dart';
import 'package:rivergentest/product_response.dart';

void main() {
  runApp(const ProviderScope(child: MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends HookConsumerWidget {
  final String title;
  const MyHomePage({super.key, required this.title});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final scroll = useScrollController();
    useEffect(() {
      print("scroll controller");
      scroll.addListener(() {
        final b = ref.watch(showBackToTopProvider);
        if (!b && scroll.offset > 400) {
          ref.read(showBackToTopProvider.notifier).setShow();
        }
      });
      return null;
    }, ['scroll']);
    final plist = ref.watch(productListProvider);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(title),
      ),
      body: plist.maybeWhen(
        orElse: () => const Center(
          child: Text("something wrong."),
        ),
        loading: () => const Center(child: CircularProgressIndicator()),
        data: (data) {
          return ListView.custom(
            controller: scroll,
            childrenDelegate: SliverChildBuilderDelegate((context, index) {
              if (index == data.length) {
                ref.read(productListProvider.notifier).loadMore();
                return data.length == totalProduct
                    ? const Center(child: Text("no more"))
                    : const SizedBox(
                        height: 40,
                        width: double.infinity,
                        child: Center(
                          child: Center(
                              child: LoadingIndicator(
                            indicatorType: Indicator.ballPulse,
                            colors: [Colors.blue, Colors.black, Colors.blue],
                          )),
                        ),
                      );
              } else {
                return ProductCard(product: data[index]);
              }
            }, childCount: data.length + 1),
          );
        },
      ),
      floatingActionButton: Consumer(
        builder: (context, ref, child) {
          final show = ref.watch(showBackToTopProvider);
          return show
              ? FloatingActionButton(
                  onPressed: () {
                    scroll
                        .animateTo(0,
                            duration: const Duration(seconds: 1),
                            curve: Curves.linear)
                        .then((value) {
                      ref.read(showBackToTopProvider.notifier).setFalse();
                    });
                  },
                  child: const Icon(Icons.arrow_circle_up_outlined))
              : const SizedBox();
        },
      ),
    );
  }
}

class ProductCard extends StatelessWidget {
  final Product product;
  const ProductCard({super.key, required this.product});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        //
        Navigator.push(context, MaterialPageRoute(
          builder: (context) {
            return ProductDetail(
              productId: product.id ?? 1,
            );
          },
        ));
      },
      child: Card(
        margin: const EdgeInsets.symmetric(vertical: 2),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(0)),
        elevation: 4,
        child: Column(
          children: [
            Image.network(
              product.thumbnail ?? "",
              width: double.infinity,
              height: 200,
              fit: BoxFit.cover,
              errorBuilder: (context, error, stackTrace) {
                return Container(
                  height: 200,
                  alignment: Alignment.center,
                  child: const Icon(Icons.image),
                );
              },
            ),
            Padding(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: Text(
                  "${product.title}",
                  style: const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                )),
            Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      "DESCRIPTION",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                    ),
                    Row(
                      children: [
                        const Icon(
                          Icons.star,
                          color: Colors.amber,
                        ),
                        const SizedBox(
                          width: 8,
                        ),
                        Text("${product.rating}"),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
                child: Text(
                  product.description ?? "unknown",
                  textAlign: TextAlign.start,
                  maxLines: 3,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Expanded(
                      child: Wrap(
                    spacing: 8,
                    children: [
                      Chip(
                        avatar: const Icon(Icons.category),
                        label: Text("${product.category}"),
                        side: BorderSide.none,
                      ),
                      Chip(
                          label: Text("${product.stock}"),
                          avatar: const Icon(Icons.production_quantity_limits),
                          side: BorderSide.none),
                    ],
                  )),
                  const Chip(
                    label: Text("See More"),
                    avatar: Icon(Icons.more),
                    side: BorderSide.none,
                  )
                ],
              ),
            ),
            const SizedBox(
              height: 16,
            ),
          ],
        ),
      ),
    );
  }
}
