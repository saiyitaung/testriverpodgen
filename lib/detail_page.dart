import 'package:card_swiper/card_swiper.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:rivergentest/product_provider.dart';

class ProductDetail extends HookConsumerWidget {
  final int productId;
  const ProductDetail({super.key, required this.productId});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final pdetail = ref.watch(productDetailProvider(productId: productId));

    return Scaffold(
      appBar: AppBar(
        title: const Text("Detail"),
      ),
      body: pdetail.maybeWhen(
        orElse: () => const Center(
          child: Text("something wrong."),
        ),
        loading: () => const Center(child: CircularProgressIndicator()),
        data: (data) {
          return SingleChildScrollView(
            child: Column(
              children: [
                Text(
                  "${data.title}",
                  style: TextStyle(
                      color: Theme.of(context).primaryColor,
                      fontSize: 24,
                      fontWeight: FontWeight.w500),
                ),
                SizedBox(
                  height: 300,
                  width: double.infinity,
                  child: Swiper(
                    itemCount: data.images?.length ?? 0,
                    layout: SwiperLayout.TINDER,
                    viewportFraction: .5,
                    itemHeight: 300,
                    itemWidth: double.infinity,
                    itemBuilder: (context, index) {
                      return Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          border: Border.all(width: 2, color: Colors.grey),
                        ),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(8),
                          child: Image.network(
                            data.images?[index] ?? "",
                            fit: BoxFit.cover,
                            height: 300,
                            width: double.infinity,
                            errorBuilder: (context, error, stackTrace) {
                              return Container(
                                color: Colors.red[300],
                                child: const Center(child: Icon(Icons.image)),
                              );
                            },
                          ),
                        ),
                      );
                    },
                  ),
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 20, vertical: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Chip(
                            avatar: const Icon(Icons.category),
                            label: Text("${data.category}"),
                            side: BorderSide.none,
                          ),
                          Chip(
                              label: Text("${data.stock}"),
                              avatar:
                                  const Icon(Icons.production_quantity_limits),
                              side: BorderSide.none),
                          Chip(
                              label: Text("${data.rating}"),
                              avatar: const Icon(Icons.star),
                              side: BorderSide.none),
                        ],
                      )),
                ),
                const Align(
                  alignment: Alignment.centerLeft,
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                    child: Text(
                      "DESCRIPTION",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 20, vertical: 10),
                    child: Text(
                      "${data.description}",
                    ),
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
